﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bouclierBonus : MonoBehaviour
{
    public int bouclierhp = 5;

    public Animator animbouclier;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {// Change la couleur^et l'anim du bouclier quand il lui reste 1hp
        if (bouclierhp <=1)
        {
            animbouclier.SetBool("bouclier1hp", true);
        }

        if (bouclierhp <= 0)
        {
            gameObject.SetActive(false);
            bouclierhp = 5;
            animbouclier.SetBool("bouclier1hp", false);
        }
        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {// Set up la vie du bouclier
        if (other.gameObject.CompareTag("Monster") && other.gameObject.CompareTag("MissileMonster"))
        {
            Destroy(other);
            bouclierhp--;
        }
    }
}
