﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bonusCol : MonoBehaviour
{
    public BonusUi bonusUi;

    public PlayerStats scorePlayer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {// Set up les power up
        if (other.gameObject.CompareTag("Player"))
        {
            PlayerStats.score = +500;
            bonusUi.powerup ++;
            if (bonusUi.powerup == 7)
            {
                bonusUi.powerup = 1;
                
            }
          
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
