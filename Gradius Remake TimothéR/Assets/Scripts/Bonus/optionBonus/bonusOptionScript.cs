﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bonusOptionScript : MonoBehaviour
{
    public float distance;
    public Transform player;
    public float speed;
    public Rigidbody2D rgbd, rgbd2;
    public Transform firepoint;
    public GameObject missile1Option, missile2Option;
    public PlayerShoot bonus;
    //Bonus Missile//

    public GameObject missileBonusOption;
    public Vector2 bonusMissilemooveOption;
  
    //Bonus Double//
   
    public GameObject doubleBonusOption;
    public Vector2 bonusDoublemooveOption;

    //Bonus Laser//
    public GameObject bonuslaser1option, bonuslaser2Option;
    public Rigidbody2D rgbLaser1Option, rgbdlaser2Option;
    public float laserSpeed;

    // Start is called before the first frame update
    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {
        transform.position = (transform.position - player.transform.position).normalized * distance + player.position;
        if (Input.GetKeyDown(KeyCode.F))
        {
            //tire des missiles
            if (missile1Option.activeSelf == false && BonusUi.laser == false)
            {
                // Tir standard
                missile1Option.SetActive(true);
                missile1Option.transform.position = firepoint.position;
                rgbd.velocity = transform.right * speed;
            }
            else if (missile2Option.activeSelf == false && BonusUi.laser == false)
            {
                missile2Option.SetActive(true);
                missile2Option.transform.position = firepoint.position;
                rgbd2.velocity = transform.right * speed;
            } else if (BonusUi.laser && bonuslaser1option.activeSelf == false )
            {// Bonus Laser
                bonuslaser1option.SetActive(true);
                bonuslaser1option.transform.position = firepoint.position;
                rgbLaser1Option.velocity = transform.right * laserSpeed;
            }
            else if (BonusUi.laser && bonuslaser2Option.activeSelf == false)
            {
                bonuslaser2Option.SetActive(true);
                bonuslaser2Option.transform.position = firepoint.position;
                rgbdlaser2Option.velocity = transform.right * laserSpeed;
            }
            if (bonus.bonusMissile && missileBonusOption.activeSelf == false)
            {// Missile bonus qui vont vers le bas
                missileBonusOption.SetActive(true);
                bonusMissilemooveOption += Vector2.down;
                bonusMissilemooveOption += Vector2.right;
                missileBonusOption.transform.position = firepoint.position;
                
            }
            if (bonus.bonusDouble && doubleBonusOption.activeSelf == false)
            {// Missile bonus qui va vers le haut
                doubleBonusOption.SetActive(true);
                doubleBonusOption.transform.position = firepoint.position;
                bonusDoublemooveOption += Vector2.up;
                bonusDoublemooveOption += Vector2.right;
              
            }
        }
        doubleBonusOption.transform.Translate(bonusDoublemooveOption.normalized * (10 * Time.deltaTime));
        missileBonusOption.transform.Translate( bonusMissilemooveOption.normalized * (7 * Time.deltaTime));
    }
    public void OnBecameInvisible()
    {// Permet de désactiver les objects quand ils ne sont plus visible a la caméra
        bonuslaser1option.SetActive(false);
       bonuslaser2Option.SetActive(false);
        missile1Option.SetActive(false);
        missile2Option.SetActive(false);
       missileBonusOption.SetActive(false);
        doubleBonusOption.SetActive(false);
    }
}