﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusUi : MonoBehaviour
{
    public Image[] bonus;

    public Sprite[] bonusSelected;
    public Sprite[] bonusdeselected;
    public int powerup = 0;

    public PlayerMoovement playermoove;

    public PlayerShoot playershoot;

    public bool activateMissile;
    public bool activateDouble;

    public static bool laser;

    public GameObject bonusBouclier;


    public GameObject bonusOption1;
    public GameObject bonusOption2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {// Systeme de sélection déselection des powerup UI
        if (powerup == 1) // Speedup
        {
            bonus[5].sprite = bonusdeselected[5];
            if (bonusBouclier.activeSelf)
            {
                bonus[5].sprite = bonusdeselected[6];
            }
            bonus[0].sprite = bonusSelected[0];                    //SpeedUp
            if (Input.GetKeyDown(KeyCode.D) && powerup == 1)
            {
                playermoove.speed  ++;
                bonus[0].sprite = bonusdeselected[0];
                powerup = 0;
            }
        }

        if (powerup == 2 )// Bonus Missile bas
        {
            if (!activateMissile)
            {
                bonus[1].sprite = bonusSelected[1];
            }

            else if (activateMissile)
            {
                bonus[1].sprite = bonusSelected[6];
            }
                                                                     // Bonus MissileBas
           
            bonus[0].sprite = bonusdeselected[0];
            if (Input.GetKeyDown(KeyCode.D) && powerup == 2 && !activateMissile)
            {
                playershoot.bonusMissile = true;
                bonus[1].sprite = bonusdeselected[6];
                powerup = 0;
                activateMissile = true;                        
            }
         
        }
        if (powerup == 3)// Bonus Missile Haut (Double)
        {
            if (!activateDouble)
            {
                bonus[2].sprite = bonusSelected[2];
            }

            if (activateDouble)
            {
                bonus[2].sprite = bonusSelected[6];
            }
            if (!activateMissile)
            {                                                                                        
                bonus[1].sprite = bonusdeselected[1];                                        
            }else if (activateMissile)
            {
                bonus[1].sprite = bonusdeselected[6];
            }
            if (Input.GetKeyDown(KeyCode.D) && powerup == 3 && !activateDouble)           // Bonus MissileHaut
            {
                playershoot.bonusDouble = true;
                bonus[2].sprite = bonusdeselected[6];
                powerup = 0;
                activateDouble = true;
                bonus[3].sprite = bonusdeselected[3];
                laser = false;
            }
        }
        if (powerup == 4)// Bonus laser 
        {
            if (!activateDouble)
            {
                bonus[2].sprite = bonusdeselected[2];
            }else if (activateDouble)
            {
                bonus[2].sprite = bonusdeselected[6];
            }                       

            if (laser)
            {
                bonus[3].sprite = bonusSelected[6];
            }else if (!laser)                            // Bonus laser
            {
                bonus[3].sprite = bonusSelected[3];
            }
            if (Input.GetKeyDown(KeyCode.D) && powerup == 4 && !laser)
            {
                laser = true;
                bonus[3].sprite = bonusdeselected[6];
                playershoot.bonusDouble = false;
                activateDouble = false;
                bonus[2].sprite = bonusdeselected[2];
                powerup = 0;
            }
           
        }
        if (powerup == 5)// Bonus Option
        {
            if (laser)
            {
                bonus[3].sprite = bonusdeselected[6];
            }else if (!laser)
            {
                bonus[3].sprite = bonusdeselected[3];
            }
            bonus[4].sprite = bonusSelected[4];
            if (Input.GetKeyDown(KeyCode.D) && powerup == 5 && bonusOption1.activeSelf == false)
            {
                bonusOption1.SetActive(true);
                bonus[4].sprite = bonusdeselected[4];
                powerup = 0;
            }

            if (Input.GetKeyDown(KeyCode.D) && powerup == 5 && bonusOption1.activeSelf)
            {
                bonusOption2.SetActive(true);
                bonus[4].sprite = bonusdeselected[6];
                powerup = 0;
            }

            if (powerup == 5 && bonusOption1.activeSelf && bonusOption2.activeSelf)
            {
                bonus[4].sprite = bonusSelected[6];
            }
            
            
        }
        if (powerup == 6)// Bonus Bouclier
        {
            bonus[5].sprite = bonusSelected[5];
            bonus[4].sprite = bonusdeselected[4];
            if (bonusOption1.activeSelf && bonusOption2.activeSelf)
            {
                bonus[4].sprite = bonusdeselected[6];
            }
            if (Input.GetKeyDown(KeyCode.D) && powerup ==  6 && bonusBouclier.activeSelf == false)
            {
                bonusBouclier.SetActive(true);                                                        // Bonus Bouclier
                bonus[5].sprite = bonusdeselected[6];
                powerup = 0;
            }

            if (bonusBouclier.activeSelf && powerup == 6)
            {
                bonus[5].sprite = bonusSelected[6];
            }
        }
      
    }
}
