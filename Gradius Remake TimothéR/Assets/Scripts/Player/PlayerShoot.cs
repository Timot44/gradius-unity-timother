﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public float speed;

    public Rigidbody2D rgbd, rgbd2;

    public Transform firepoint;

    public GameObject missile1, missile2;
    
  //Bonus Missile//
    public bool bonusMissile;
    public GameObject missileBonus;
    public Transform missileshoot;
    public Vector2 bonusMissilemoove;
//Bonus Double//
    public bool bonusDouble;
    public GameObject doubleBonus;
    public Transform doubleshoot;
    public Vector2 bonusDoublemoove;
   //Bonus Laser//
   public GameObject bonuslaser1, bonuslaser2;
   public Rigidbody2D rgbLaser1, rgbdlaser2;
   public float laserSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {//tire des missiles
            if (missile1.activeSelf == false && BonusUi.laser==false)
            {// Tir standard
                missile1.SetActive(true);
                missile1.transform.position = firepoint.position;
                rgbd.velocity = transform.right * speed;
            }else if (missile2.activeSelf == false && BonusUi.laser == false)
            {
                missile2.SetActive(true);
                missile2.transform.position = firepoint.position;
                rgbd2.velocity = transform.right * speed;
            }
            else if (BonusUi.laser && bonuslaser1.activeSelf == false )
            {// Bonus Laser
                bonuslaser1.SetActive(true);
                bonuslaser1.transform.position = firepoint.position;
                rgbLaser1.velocity = transform.right * laserSpeed;
            }
            else if (BonusUi.laser && bonuslaser2.activeSelf == false)
            {
                bonuslaser2.SetActive(true);
                bonuslaser2.transform.position = firepoint.position;
                rgbdlaser2.velocity = transform.right * laserSpeed;
            }
           

            if (bonusMissile && missileBonus.activeSelf == false)
            {// Missile bonus qui vont vers le bas
             missileBonus.SetActive(true);
              bonusMissilemoove += Vector2.down;
              bonusMissilemoove += Vector2.right;
              missileBonus.transform.position = missileshoot.position;
              
                //rgbdMissileBonus.velocity = Vector2.up * -10;
            }
           
            if (bonusDouble && doubleBonus.activeSelf == false)
            {// Missile bonus qui va vers le haut
                doubleBonus.SetActive(true);
                doubleBonus.transform.position = doubleshoot.position;
                bonusDoublemoove += Vector2.up;
                bonusDoublemoove += Vector2.right;
                // rgbdDoubleBonus.velocity = Vector2.down * -10;
            }
          
        }
        doubleBonus.transform.Translate(bonusDoublemoove.normalized * (10 * Time.deltaTime));
        missileBonus.transform.Translate( bonusMissilemoove.normalized * (7 * Time.deltaTime));
    }
    

    public void OnBecameInvisible()
    {// Permet de désactiver les objects quand ils ne sont plus visible a la caméra
        bonuslaser1.SetActive(false);
        bonuslaser2.SetActive(false);
        missile1.SetActive(false);
        missile2.SetActive(false);
        missileBonus.SetActive(false);
        doubleBonus.SetActive(false);
    }
}
