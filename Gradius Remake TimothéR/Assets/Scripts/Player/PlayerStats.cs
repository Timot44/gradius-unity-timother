﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using  TMPro;
public class PlayerStats: MonoBehaviour
{
    public Animator animator;
    public int playerLife = 3;
    public Transform spawnpoint;
    public TextMeshProUGUI textlife;
    public  bool playerhit = false;
    public float timeRespawn = 1f;

    public static  int score = 0;
    public TextMeshProUGUI scoreText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = score.ToString();
        if (playerLife <= 0)
        {
            Dead();
        }

        if (playerhit)
        {
            timeRespawn -= Time.deltaTime;
            if (timeRespawn <= 0)
            {
                PlayerGetHit();
            }
            
        }
    }

    public void OnTriggerEnter2D(Collider2D other1)
    {// Set le score et retire 1hp au player
        if (other1.gameObject.CompareTag("Monsters"))
        {
            playerLife--;
            textlife.text = playerLife.ToString();
            playerhit = true;
            animator.SetBool("Die", true);
            animator.SetBool("stop", false);
            scoreText.text = score.ToString();
            PlayerPrefs.SetInt("Score", score);
        }
    }

    public void PlayerGetHit()
    {
        animator.SetBool("Die", false);
        transform.position = spawnpoint.position;
        PlayerPrefs.GetInt("Score");
       
    }

    public void Dead()
    {
        
       
    }
    
}
