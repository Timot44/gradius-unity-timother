﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoovement : MonoBehaviour
{
    public float speed = 10.0f;
    public Rigidbody2D rgbd;

    public Vector2 movement;
    
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        rgbd = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Bouge le player de haut en bas + droite gauche
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        
        animator.SetFloat("Vertical", movement.y);
        if (movement.x !=0 )
        {
            animator.SetBool("stop",true);
        }

      
    }


    private void FixedUpdate()
    {//Permet de bouger le player
        rgbd.MovePosition(rgbd.position + movement * (speed * Time.fixedDeltaTime));
    }
}
