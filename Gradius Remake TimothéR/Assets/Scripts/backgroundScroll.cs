﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backgroundScroll : MonoBehaviour
{   
    public float scrollSpeed = -5f;

    public Vector2 startPos;
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {// Permet de scroll le background
       
        transform.position = startPos + Vector2.left * (scrollSpeed *Time.time);
    }
}
