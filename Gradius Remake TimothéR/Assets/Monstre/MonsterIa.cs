﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterIa : MonoBehaviour
{
    public PlayerStats scoreplayer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D other1)
    {
        if (other1.gameObject.CompareTag("Player") || other1.gameObject.CompareTag("Bonus"))
        {
            PlayerStats.score = +100;
            Destroy(gameObject);
            
        }
    }
}
